import sys, os, csv, codecs, re, unicodedata
from operator import itemgetter
import operator
#import numpy as np
import utility, errorcheck

def main(targetcsv = ''): 

    ### Read csv list from csvlist ###
    temp = [line.strip() for line in open('csvlist')]  ## Original: csvlist for check, ## reviselist for revised data
    csvlist = []
    #if len(sys.argv) > 1: 
    if targetcsv != '':
        ## filter target csv category ###
        #targetcsv = sys.argv[1]
        for e, i in enumerate(temp):
            if targetcsv in i:
                csvlist.append(i)
    else:
        csvlist = temp
    
    Stat = {}
    for completename in csvlist:
        ### get csv category, and split-csv-file ###
        foldername = completename.split('/')[-2]
        csvname = completename.split('/')[-1].split('.')[0]        
        print ("\nProcessing ...%s %s" %(foldername, csvname))
        ### Read data ###
        Object, colnames, data = utility.Read(completename, foldername)
        ### Remove 'firld' to 'field' ###
        if foldername == '內容主題-生物':
            print ("Remove_Subject_firld")
            colnames, data = errorcheck.Remove_Subject_firld(Object, colnames, data)
        
        ### Start to revised ###
        print ("Start to revised")
        ### Revised Part ###
        Object, data, error_4022 = errorcheck.Revised_error_4022(Object, colnames, data)
        Object, data, error_html = errorcheck.Remove_Title_HTMLTag(Object, colnames, data)
        Object, data, error_nullvalue = errorcheck.Remove_nullvalue(Object, colnames, data)
        
        ### Delete Part ###
        del_linenum_v3 = []
        del_linenum_v3 += errorcheck.Delete_error_4023(Object, colnames, data)
        del_linenum_v3 += errorcheck.Delete_error_4024(Object, colnames, data)
        del_linenum_v3 += errorcheck.TitleProblem(Object, colnames, data)
        
        print ("Start to Check")
        MetaDesc_line, MetaDesc_oid = errorcheck.Delete_NO_MetaDesc(Object, colnames, data)
        del_linenum_v31 = del_linenum_v3 + MetaDesc_line
        
        ICON_line, ICON_oid = errorcheck.Delete_NO_Icon(Object, colnames, data)
        
        #if del_linenum != [] or error_4022 != [] or error_html != [] or error_nullvalue != []:
        #    print ("Error Found")
    
        print ('Total %d %d %d %d' %(len(Object), len(Object) - MetaDesc_oid, MetaDesc_oid, ICON_oid))
        Stat[csvname] = {'OID': len(Object), 'MetaDesc': len(Object) - MetaDesc_oid, 'NO_MetaDesc': MetaDesc_oid, 'MetaDesc_but_no_ICON': ICON_oid}

        ### delete the oid should be remove ###
        newdata_v3 = []
        newdata_v31 = []
        Metadata = [data[0]]
        Icondata = [data[0]]

        for lineid in range(len(data)):
            if lineid not in del_linenum_v3:
                newdata_v3.append(data[lineid])
            if lineid not in del_linenum_v31:
                newdata_v31.append(data[lineid])
            if lineid in MetaDesc_line:
                Metadata.append(data[lineid])
            if lineid in ICON_line:
                Icondata.append(data[lineid])

        utility.CSVWRITETOFILE(newdata_v3, "revised-csv-v3-split-by-project/%s/%s.csv" %(foldername, csvname))

        utility.CSVWRITETOFILE(newdata_v31, "revised-csv-v31-split-by-project/%s/%s.csv" %(foldername, csvname))
        if MetaDesc_oid > 0:
            utility.CSVWRITETOFILE(Metadata, "revised-csv-v32-split-by-project/%s/%s.csv" %(foldername, csvname))
        if ICON_oid > 0:
            utility.CSVWRITETOFILE(Icondata, "revised-csv-v33-split-by-project/%s/%s.csv" %(foldername, csvname))
    
    if targetcsv != '':
        return Stat

if __name__ == "__main__":
    
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main()
