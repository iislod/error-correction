#coding=utf-8
import sys, os
import csv, codecs
from dateutil import parser
import time
import datetime
import http.client
import re

import unicodedata
import operator
import utility

### Revised the error code 4022
### Fixed the problem of mistaken using ":" between agent
### Besides fixed record have ":" end of content
def Revised_error_4022(Object, colnames, data):
    
    AGENT = [line.strip() for line in open("AGENT.csv")]
    Recordid = 0
    for e, c in enumerate(colnames):
        if c == 'Record':
            Recordid = e

    OIDSET = []
    for i in Object:
        for e, j in enumerate(Object[i]['Record']):
            token = j[1].split(':')
            for t in token[:-1]:
                if t in AGENT:
                    continue
                if '：' in t:
                    newstring = j[1].replace('：', ":")
                    Object[oid]['Record'][e] = (j[0], newstring)
                    data[int(j[0])][Recordid] = newstring
                    OIDSET.append(oid)

    return Object, data, OIDSET

def Find_error_4022(Object, colnames, data):
    
    AGENT = [line.strip() for line in open("AGENT.csv")]
    Recordid = 0
    for e, c in enumerate(colnames):
        if c == 'Record':
            Recordid = e

    OIDSET = {}
    for i in Object:
        for e, j in enumerate(Object[i]['Record']):
            token = j[1].split(':')
            for t in token[:-1]:
                if t in AGENT:
                    continue
                if '：' in t:
                    newstring = j[1].replace('：', ":")
                    Object[oid]['Record'][e] = (j[0], newstring)
                    data[int(j[0])][Recordid] = newstring
                    OIDSET[oid] = 1

    return OIDSET.keys(), "Error(4022)"

### Delete the OID object who has continuous ":" and whose length smaller than 3
def Delete_error_4023(Object, colnames, data):
    
    OIDLIST = []
    del_linenum = []

    for i in Object:
        for j in Object[i]['Record']:
            if '::' in j[1]:
                OIDLIST.append(i)
            elif j[1].endswith(':'):
                OIDLIST.append(i)
            
            if '典藏機構與計畫' in j[1] or '內容主題' in j[1]:
                if len(j[1].split(':')) < 3:
                    OIDLIST.append(i)

    OIDLIST = list(set(OIDLIST))
    
    for oid in sorted(OIDLIST):
        line_iter = int(Object[oid]['DAURI'][0][0])
        while data[line_iter][0] == oid and line_iter < len(data):
            del_linenum.append(line_iter)
            line_iter += 1

    return del_linenum

def Find_error_4023(Object, colnames, data):
    
    OIDLIST = []

    for i in Object:
        for j in Object[i]['Record']:
            if '::' in j[1]:
                OIDLIST.append(i)
            elif j[1].endswith(':'):
                OIDLIST.append(i)
            
            if '典藏機構與計畫' in j[1] or '內容主題' in j[1]:
                if len(j[1].split(':')) < 3:
                    OIDLIST.append(i)

    OIDLIST = list(set(OIDLIST))
    return OIDLIST, "Error(4023)"

### Delete the OID object who has NA in record or '?' in record
def Delete_error_4024(Object, colnames, data=[]):
    
    OIDLIST = []
    del_linenum = []
    
    for i in Object:
        for j in Object[i]['Record']:
            if j[1] == 'N' or j[1] == 'A' or j[1] == 'n' or j[1] == '' or j[1] == ' ' or j[1] == '\n':
                OIDLIST.append(i)
            elif '?' in j[1] or '？' in j[1] or '\n' in j[1]:
                OIDLIST.append(i)

    diff = []
    for oid in OIDLIST:
        line_iter = int(Object[oid]['DAURI'][0][0])
        while data[line_iter][0] == oid and line_iter < len(data):
            del_linenum.append(line_iter)
            line_iter += 1
            
    return del_linenum

def Find_error_4024(Object, colnames, data=[]):
    
    OIDLIST = []
    
    for i in Object:
        for j in Object[i]['Record']:
            if j[1] == 'N' or j[1] == 'A' or j[1] == 'n' or j[1] == '' or j[1] == ' ' or j[1] == '\n':
                OIDLIST.append(i)
            elif '?' in j[1] or '？' in j[1] or '\n' in j[1]:
                OIDLIST.append(i)

    return OIDLIST, "Error(4024)"

### Delete the OID object whose Title is null or has '?' in content
def TitleProblem(Object, colnames, data):
    
    OIDLIST = []
    del_linenum = []

    for oid in Object:
        if 'Title' not in Object[oid]:
            continue
        for j in Object[oid]['Title']:
            if j[1] == '':
                OIDLIST.append(oid)
            if '?' in j[1] and j[1][-1] != '?':
                OIDLIST.append(oid)
   
    for oid in OIDLIST:
        line_iter = int(Object[oid]['DAURI'][0][0])
        while line_iter < len(data) and data[line_iter][0] == oid:
            del_linenum.append(line_iter)
            line_iter += 1

    return del_linenum

### Delete the OID object whose Title is null or has '?' in content
def Find_TitleProblem(Object, colnames, data):
    
    OIDLIST = {}

    for oid in Object:
        if 'Title' not in Object[oid]:
            continue
        for j in Object[oid]['Title']:
            if j[1] == '':
                print (oid)
                OIDLIST[oid] = 1
            if '?' in j[1] and j[1][-1] != '?':
                OIDLIST[oid] = 1

    return OIDLIST.keys(), "Error(Title)"

def Delete_NO_MetaDesc(Object, colnames, data):
    
    OIDLIST = []
    del_linenum = []

    for oid in Object:
        if 'MetaDesc::license' not in Object[oid]:
            OIDLIST.append(oid)
   
    for oid in OIDLIST:
        line_iter = int(Object[oid]['DAURI'][0][0])
        while line_iter < len(data) and data[line_iter][0] == oid:
            del_linenum.append(line_iter)
            line_iter += 1

    return del_linenum, len(set(OIDLIST))

def Find_NO_MetaDesc(Object, colnames, data):
    
    OIDLIST = {}
    for oid in Object:
        if 'MetaDesc::license' not in Object[oid]:
            OIDLIST[oid] = 1

    return OIDLIST.keys(), "Error(NO MetaDesc)"

def Delete_NO_Icon(Object, colnames, data):
    
    OIDLIST = []
    del_linenum = []

    for oid in Object:
        if 'MetaDesc::license' in Object[oid] and 'ICON::license' not in Object[oid]:
            OIDLIST.append(oid)
   
    for oid in OIDLIST:
        line_iter = int(Object[oid]['DAURI'][0][0])
        while line_iter < len(data) and data[line_iter][0] == oid:
            del_linenum.append(line_iter)
            line_iter += 1

    return del_linenum, len(set(OIDLIST))

def Find_NO_Icon(Object, colnames, data):
    
    OIDLIST = {}
    for oid in Object:
        if 'MetaDesc::license' in Object[oid] and 'ICON::license' not in Object[oid]:
            OIDLIST[oid] = 1

    return OIDLIST.keys(), "Error(No Icon)"

### Fixed the problem in Title
### Remove the html tag in title
def Remove_Title_HTMLTag(Object, colnames, data):
    
    Titleid = ''
    for e, c in enumerate(colnames):
        if c == 'Title':
            Titleid = e
    
    OIDLIST = []
    for oid in Object:
        if 'Title' not in Object[oid]:
            continue
        for e, j in enumerate(Object[oid]['Title']):
            if utility.MatchFormat(j[1], '.*<.*>.*</.*>.*'):
                newstring = re.sub("<.*?>", "", j[1])
                Object[oid]['Title'][e] = (j[0], newstring)
                data[int(j[0])][Titleid] = newstring
                OIDLIST.append(oid)

    return Object, data, OIDLIST

def Find_Title_HTMLTag(Object, colnames, data):
    
    Titleid = ''
    for e, c in enumerate(colnames):
        if c == 'Title':
            Titleid = e
    
    OIDLIST = {}
    for oid in Object:
        if 'Title' not in Object[oid]:
            continue
        for e, j in enumerate(Object[oid]['Title']):
            if utility.MatchFormat(j[1], '.*<.*>.*</.*>.*'):
                OIDLIST[oid] = 1

    return OIDLIST.keys(), "Error(Title::HTMLTag)"

### Construct the pair between field and content
### For example, Title and Title::field
def GetPairlist(colnames):
    
    colindex = {i:e for e, i in enumerate(colnames)}
    Pair = {}
    for e, c in enumerate(colnames):
        if '::field' in c or '::dwc' in c:
            target = c.split('::')[0]
            if target in colnames:
                Pair[c] = colindex[target]
            
    return Pair

### Remove null value in content
def Remove_nullvalue(Object, colnames, data):
    
    Pair = GetPairlist(colnames)
    OIDLIST = []

    for oid in Object:
        for (colid, col) in enumerate(colnames[1:], 1):
            if col in Object[oid] and col in Pair:
                for e, (linenum, content) in enumerate(Object[oid][col]):
                    if (content == '' or content == ' ') and (data[int(linenum)][Pair[col]] == ' ' or data[int(linenum)][Pair[col]] == ''):
                        data[int(linenum)][colid] = ''
                        OIDLIST.append(oid)

    return Object, data, OIDLIST 

### Remove the problem only in 內容主題：生物
### Fixed the typing problem Subject::firld to Subject::field
def Remove_Subject_firld(Object, colnames, data):

    if 'Subject::firld' not in colnames:
        return colnames, data
    
    for e, c in enumerate(colnames):
        if c == 'Subject::firld':
            firld_id = e
        if c == 'Subject::field':
            field_id = e

    for row in range(len(data)-1):
        oid = data[row+1][0]
        if 'Subject::firld' in Object[oid]:
            data[row+1][field_id] = data[row+1][firld_id]
        del data[row+1][firld_id]    
    
    del data[0][firld_id]
    return colnames, data
            
if __name__ == "__main__":
    pass
