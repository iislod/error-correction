#coding=utf-8
import sys, os
import csv, codecs, re
import unicodedata
from collections import Counter
import operator

### Read Data in CSV FILE ###
### The function will return 3 variables.
### First, csv object, which is stored in a dictionary structure.
### Object -> 'OID': { 'OID': oid,
###                    'DAURI': [(lin_number, content)],
###                    'Title': [(lin_number, content), ..., (lin_number, content)],
###                    'Title::field': [(lin_number, content), ..., (lin_number, content)],
###                    ...: ...,
###                    ...: ...,
###                 }
### Second, column, csv header, stored in a list
### Third, data, raw data of csv file, stored in a 2D list.

def Read(FileName, csvname):

    Obj = {}
    data = []
    with open(FileName, newline='') as f:
        dat = csv.reader(f, delimiter=',', quotechar='"')
        for i in dat:
            data.append(i)

    colnames = data[0]
    colnames[0] = "OID"

    objid = 'ID'
    for linenum, line in enumerate(data[1:], 1):
        for e, x in enumerate(line):
            if x != '':
                x = x.replace(",", "，")
                if e == 0:
                    if x not in Obj:
                        objid = x
                        Obj[objid] = {}
                        Obj[objid][colnames[e]] = (str(linenum), x)
                        Obj[objid]['CSVDAT'] = csvname
                else:
                    if colnames[e] not in Obj[objid]:
                        Obj[objid][colnames[e]] = []
                    Obj[objid][colnames[e]].append((str(linenum), x))

    return Obj, colnames, data

### Write data into csv file ###
def CSVWRITETOFILE(X, filename):
    with open(filename, "w", newline='') as f:
        f.write('\ufeff')
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerows(X)

### Test a string is a number string or not ###
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
    
    try:
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    
    return False

### Segment to document to sentence
def Segment(textstring):
    textstring = unicodedata.normalize('NFKC', textstring)
    textstring = textstring.replace('。','.')
    
    return re.split('\W', textstring, flags=re.UNICODE)

### Transform URL to xml url
def TransToXML(url):
    ### URL : http://catalog.digitalarchives.tw/item/00/00/46/14.html ###
    ### TO: http://140.109.18.115/showxml/00/00/46/14.xml ###
    
    temp = url.split('/')
    item = ""
    get = 0
    for i in temp:
        if i == "item":
            get = 1
            continue
        if get == 1:
            item += i + '/'
    
    newurl = "http://140.109.18.115/showxml/" + item.split('.')[0] + '.xml'
    return newurl

### Match string format
def MatchFormat(string, formats):
    r = re.compile(formats)
    if r.match(string):
        return 1
    else:
        return 0
