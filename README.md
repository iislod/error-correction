# Tool for Error Correction

author: Hsin-Ping Chen

# Introduction

This tool is designed to revise csv files converted from xml files of Union Catalog of Digital Archives Taiwan.

**Note that this tool is only for csv files spllited by projects.**

* Input: csv files converted from xml files of Union Catalog of Digital Archives Taiwan

* Output: revised csv files

# Dependencies

* Python 3.4

* Python dependencies

```
pip3 install -r requirements.txt
```

* Only tested on Ubuntu (>= 14.04)

```
sudo apt-get install build-essential python3-dev
```

# Usage

1. Modify the file `csvlist` with **full paths** of input csv files.

2. Create output directories named `revised-csv-v3-split-by-project`, `revised-csv-v31-split-by-project`, `revised-csv-v32-split-by-project`, and `revised-csv-v33-split-by-project` with sub directories in the name of 14 domains (ex. `內容主題-XXX`).

   * The meanings of revised-csv-v3, v31, v32, v33:
     * CSV Version3: all oids.
     * CSV Version 3-1: all with "MetaDesc license".
     * CSV Version 3-2: all without "MetaDesc license".
     * CSV Version 3-3: all with "MetaDesc license", but no "Icon license”.

3. Run `python3 revised_error.py [domain]` (domain=內容主題-XXX is optional)

4. Results can be found in the directories mentioned in step 2.
